export const trading = (token) => {
  return fetch(`/query?code=CertSM&status=Trading`, {  
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
  });
}